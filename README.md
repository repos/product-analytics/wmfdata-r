
<!-- README.md is generated from README.Rmd. Please edit that file -->

# wmfdata for R

<!-- badges: start -->

[![License:
BSD_3\_clause](https://img.shields.io/badge/license-BSD_3_clause-blue.svg)](LICENSE.md)
[![](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
[![Project Status: WIP - Initial development is in progress, but there
has not yet been a stable, usable release suitable for the
public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)
<!-- badges: end -->

`wmfdata` (version 2) is an R package for analyzing Wikimedia data on
Wikimedia’s
[non-public](https://wikitech.wikimedia.org/wiki/Analytics/Data_access#Production_access)
[analytics
clients](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Clients)by
wrapping [wmfdata for
Python](https://github.com/wikimedia/wmfdata-python).

If you used [the legacy version](https://github.com/wikimedia/wmfdata-r)
you are encouraged to switch to *this version* to benefit from the
maintenance and improvements that wmfdata-python receives.

**NOTE**: This package is currently a work-in-progress and much of the
functionality is not yet implemented.

-   [ ] Querying data
    -   [ ] `hive_run()` – wrapper for `wmfdata.hive.run()`
    -   [ ] `presto_run()` – wrapper for `wmfdata.presto.run()`
    -   [ ] `druid_run()`
-   [ ] Loading data
    -   [ ] `hive_load_csv()` – R-native port of
        `wmfdata.hive.load_csv()`
    -   [ ] `hive_load_data()` – load `data.frame` directly from R
        -   [ ] Allow partitioning

## Installation

You can install the development version like so:

``` r
# install.packages("remotes")
remotes::install_gitlab(
    "repos/product-analytics/wmfdata-r",
    host = "https://gitlab.wikimedia.org"
)
```

## Usage

``` r
library(wmfdata)
```

## Maintenance

This package is maintained by the Wikimedia Foundation’s [Product
Analytics](https://www.mediawiki.org/wiki/Product_Analytics) team.
